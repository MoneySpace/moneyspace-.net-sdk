using System.Security.Cryptography;
using System.Text;

namespace MoneySpaceSDK.Common.Calculators
{
    internal class HmacCalculator : IHashCalculator
    {
        public string ComputeHash(string input, string key)
        {
            var encoding = new UTF8Encoding();
            var inputBytes = encoding.GetBytes(input);
            var keyBytes = encoding.GetBytes(key);
            var hashBytes = ComputeHash(inputBytes, keyBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hashBytes.Length; i++) sb.Append(hashBytes[i].ToString("x2"));
            return sb.ToString();
        }

        private byte[] ComputeHash(byte[] input, byte[] key)
        {
            using (var hmac = new HMACSHA256(key))
            {
                var hashValue = hmac.ComputeHash(input);
                return hashValue;
            }
        }
    }
}