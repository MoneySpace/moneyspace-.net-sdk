using System;

namespace MoneySpaceSDK.Common.Configuration
{
    public class MoneySpaceConfiguration
    {
        private const string ProductionUri = "https://www.moneyspace.net/";

        /// <summary>
        ///     Creates a new <see cref="MoneySpaceConfiguration" /> instance, explicitly setting the API's base URI.
        /// </summary>
        /// <param name="secretId">Your secret id obtained from the MoneySpace portal.</param>
        /// ///
        /// <param name="secretKey">Your secret key obtained from the MoneySpace portal.</param>
        /// <param name="uri">The base URL of the MoneySpace API you wish to connect to.</param>
        public MoneySpaceConfiguration(string secretId, string secretKey, string uri = ProductionUri)
        {
            if (string.IsNullOrEmpty(secretId))
                throw new ArgumentException("Your API secret key is required", nameof(secretId));
            if (string.IsNullOrEmpty(uri)) throw new ArgumentException("The API URI is required", nameof(uri));

            SecretId = secretId;
            SecretKey = secretKey;
            Uri = uri;
        }

        /// <summary>
        ///     Gets the secret id that will be used to authenticate to the MoneySpace API.
        /// </summary>
        public string SecretId { get; }

        /// <summary>
        ///     Gets the secret key that will be used to authenticate to the MoneySpace API.
        /// </summary>
        public string SecretKey { get; }

        /// <summary>
        ///     Gets the Uri of the MoneySpace API to connect to.
        /// </summary>
        public string Uri { get; }
    }
}