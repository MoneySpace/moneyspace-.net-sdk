using MoneySpaceSDK.Merchant.Model;

namespace MoneySpaceSDK.Helpers.WebHook
{
    public interface IWebHookHelper
    {
        bool VerifyResponse(WebHookResponse response);
    }
}