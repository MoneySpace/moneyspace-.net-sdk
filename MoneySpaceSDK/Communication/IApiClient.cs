using System;
using System.Threading;
using System.Threading.Tasks;
using MoneySpaceSDK.Common.Credentials;

namespace MoneySpaceSDK.Communication
{
    internal interface IApiClient
    {
        /// <summary>
        ///     Executes a GET request to the specified <paramref name="path" />.
        /// </summary>
        /// <param name="path">The API resource path.</param>
        /// <param name="apiCredentials">The api credentials used to authenticate the request.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <typeparam name="TResult">The expected response type to be deserialized.</typeparam>
        /// <returns>A task that upon completion contains the specified API response data.</returns>
        Task<TResult> GetAsync<TResult>(string path, IApiCredentials apiCredentials,
            CancellationToken cancellationToken);

        /// <summary>
        ///     Executes a POST request to the specified <paramref name="path" />.
        /// </summary>
        /// <param name="path">The API resource path.</param>
        /// <param name="apiCredentials">The api credentials used to authenticate the request.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <param name="request">Optional data that should be sent in the request body.</param>
        /// <typeparam name="TRequest">The expected request type to be serialized.</typeparam>
        /// <typeparam name="TResult">The expected response type to be deserialized.</typeparam>
        /// <returns>A task that upon completion contains the specified API response data.</returns>
        Task<TResult> PostAsync<TRequest, TResult>(string path, IApiCredentials apiCredentials,
            CancellationToken cancellationToken,
            TRequest request);

        /// <summary>
        ///     Get url of specified request <paramref name="path" />.
        /// </summary>
        /// <param name="path">The API resource path.</param>
        /// <param name="apiCredentials">The api credentials used to authenticate the request.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the specified API request uri.</returns>
        Task<Uri> GetUrl(string path, IApiCredentials apiCredentials, CancellationToken cancellationToken);
    }
}