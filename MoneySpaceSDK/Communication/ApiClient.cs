using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MoneySpaceSDK.Common;
using MoneySpaceSDK.Common.Configuration;
using MoneySpaceSDK.Common.Credentials;
using MoneySpaceSDK.Exceptions;
using MoneySpaceSDK.Serialization;

namespace MoneySpaceSDK.Communication
{
    /// <summary>
    ///     Handles the authentication, serialization and sending of HTTP requests to MoneySpace APIs.
    /// </summary>
    internal class ApiClient : IApiClient
    {
        private readonly MoneySpaceConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ISerializer _serializer;

        /// <summary>
        ///     Creates a new <see cref="ApiClient" /> instance with the provided configuration.
        /// </summary>
        /// <param name="configuration">The MoneySpace configuration required to configure the client.</param>
        public ApiClient(MoneySpaceConfiguration configuration) : this(configuration, new JsonSerializer())
        {
        }

        /// <summary>
        ///     Creates a new <see cref="ApiClient" /> instance with the provided configuration, HTTP client and serializer.
        /// </summary>
        /// <param name="configuration">The MoneySpace configuration required to configure the client.</param>
        /// <param name="serializer">A serializer used to serialize and deserialize HTTP payloads.</param>
        public ApiClient(MoneySpaceConfiguration configuration, ISerializer serializer)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            _httpClient = new HttpClient();
        }

        public async Task<TResult> GetAsync<TResult>(string path, IApiCredentials apiCredentials,
            CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if (apiCredentials == null) throw new ArgumentNullException(nameof(apiCredentials));

            using (var httpResponse =
                await SendRequestAsync(HttpMethod.Get, path, apiCredentials, null, cancellationToken))
            {
                return await DeserializeJsonAsync<TResult>(httpResponse);
            }
        }

        public async Task<Uri> GetUrl(string path, IApiCredentials apiCredentials,
            CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if (apiCredentials == null) throw new ArgumentNullException(nameof(apiCredentials));

            var httpRequest = new HttpRequestMessage(HttpMethod.Get, GetRequestUri(path));

            await apiCredentials.AuthorizeAsync(httpRequest, null);

            return httpRequest.RequestUri;
        }

        public async Task<TResult> PostAsync<TRequest, TResult>(string path, IApiCredentials apiCredentials,
            CancellationToken cancellationToken,
            TRequest request)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if (apiCredentials == null) throw new ArgumentNullException(nameof(apiCredentials));

            using (var httpResponse =
                await SendRequestAsync(HttpMethod.Post, path, apiCredentials, request, cancellationToken))
            {
                return await DeserializeJsonAsync<TResult>(httpResponse);
            }
        }

        private async Task<HttpResponseMessage> SendRequestAsync(HttpMethod httpMethod, string path,
            IApiCredentials apiCredentials, object request, CancellationToken cancellationToken)
        {
            if (string.IsNullOrEmpty(path))
                throw new ArgumentNullException(nameof(path));

            var httpRequest = new HttpRequestMessage(httpMethod, GetRequestUri(path));

            httpRequest.Headers.UserAgent.ParseAdd(Constants.UserAgent);

            if (request != null)
            {
                var temp = new FormUrlEncodedContent(_serializer.ToKeyValue(request));
                var encoded = await temp.ReadAsStringAsync();
                httpRequest.Content = new StringContent(HttpUtility.UrlDecode(encoded));
                httpRequest.Content.Headers.ContentType =
                    MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");
            }

            await apiCredentials.AuthorizeAsync(httpRequest, _serializer.ToKeyValue(request));

            var httpResponse = await _httpClient.SendAsync(httpRequest, cancellationToken);

            await ValidateResponseAsync(httpResponse);

            return httpResponse;
        }

        private async Task<T> DeserializeJsonAsync<T>(HttpResponseMessage httpResponse)
        {
            if (httpResponse.Content == null)
                return default(T);

            var json = await httpResponse.Content.ReadAsStringAsync();
            return _serializer.Deserialize<List<T>>(json)[0];
        }

        private Task ValidateResponseAsync(HttpResponseMessage httpResponse)
        {
            if (!httpResponse.IsSuccessStatusCode)
                switch (httpResponse.StatusCode)
                {
                    case HttpStatusCode.NotFound:
                        throw new MoneySpaceResourceNotFoundException();
                    default:
                        throw new MoneySpaceApiException(httpResponse.StatusCode);
                }

            return Task.CompletedTask;
        }

        private Uri GetRequestUri(string path)
        {
            var baseUri = new Uri(_configuration.Uri);
            Uri.TryCreate(baseUri, path, out var uri);

            return uri;
        }
    }
}