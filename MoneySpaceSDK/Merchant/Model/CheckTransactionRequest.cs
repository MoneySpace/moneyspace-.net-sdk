using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CheckTransactionRequest
    {
        [JsonProperty("transactionID")]
        [Required]
        public string TransactionId { get; set; }

        [JsonProperty("timeHash")]
        public DateTime TimeHash { get; set; }
    }
}