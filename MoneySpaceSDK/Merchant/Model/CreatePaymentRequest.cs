using System;
using System.ComponentModel.DataAnnotations;
using MoneySpaceSDK.Merchant.Model.Enum;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CreatePaymentRequest
    {
        [JsonProperty("firstname")]
        [Required]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        [Required]
        public string LastName { get; set; }

        [JsonProperty("email")]
        [EmailAddress]
        public string Email { get; set; }

        [JsonProperty("phone")]
        [Phone]
        public string Phone { get; set; }

        [JsonProperty("currency")]
        public string Currency => "THB";

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("message")]
        [MaxLength(100)]
        public string Message { get; set; }

        [JsonProperty("feeType")]
        [Required]
        public FeeType FeeType { get; set; }

        [JsonProperty("customer_order_id")]
        [Required]
        public string CustomerOrderId { get; set; }

        [JsonProperty("gatewayType")]
        [Required]
        public GatewayType GatewayType { get; set; }

        [JsonProperty("amount")]
        [Required]
        public double Amount { get; set; }

        [JsonProperty("timeHash")]
        public DateTime TimeHash { get; set; }

        [JsonProperty("successUrl")]
        public string SuccessUrl { get; set; }

        [JsonProperty("failUrl")]
        public string FailUrl { get; set; }

        [JsonProperty("cancelUrl")]
        public string CancelUrl { get; set; }
    }
}