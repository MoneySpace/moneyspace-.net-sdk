using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MoneySpaceSDK.Merchant.Model.Enum
{
    [JsonConverter(typeof(StringEnumConverter))]
    [DataContract]
    public enum StartTerm
    {
        [EnumMember(Value = "3")]
        Three = 3,
        
        [EnumMember(Value = "4")]
        Four = 4,
        
        [EnumMember(Value = "5")]
        Five = 5,
        
        [EnumMember(Value = "6")]
        Six = 6,
        
        [EnumMember(Value = "7")]
        Seven = 7,
        
        [EnumMember(Value = "8")]
        Eight = 8,
        
        [EnumMember(Value = "9")]
        Nine = 9,
        
        [EnumMember(Value = "10")]
        Ten = 10
    }
}