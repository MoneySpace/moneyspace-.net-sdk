using System;
using System.ComponentModel.DataAnnotations;
using MoneySpaceSDK.Merchant.Model.Enum;
using Newtonsoft.Json;

namespace MoneySpaceSDK.Merchant.Model
{
    public class CreateInstallmentRequest
    {
        [JsonProperty("firstname")]
        [Required]
        public string FirstName { get; set; }

        [JsonProperty("lastname")]
        [Required]
        public string LastName { get; set; }

        [JsonProperty("email")]
        [EmailAddress]
        public string Email { get; set; }

        [JsonProperty("phone")]
        [Phone]
        public string Phone { get; set; }

        [JsonProperty("currency")]
        public string Currency => "THB";

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("message")]
        [MaxLength(100)]
        public string Message { get; set; }

        [JsonProperty("feeType")]
        [Required]
        public FeeType FeeType { get; set; }

        [JsonProperty("customer_order_id")]
        [Required]
        [MaxLength(20)]
        //[OnlyUppercaseAndNumbers]
        public string CustomerOrderId { get; set; }

        [JsonProperty("gatewayType")]
        [Required]
        public GatewayType GatewayType => GatewayType.Card;

        [JsonProperty("amount")]
        [Required]
        public double Amount { get; set; }
        
        [JsonProperty("timeHash")]
        public DateTime TimeHash { get; set; }

        [JsonProperty("successUrl")]
        [Required]
        public string SuccessUrl { get; set; }

        [JsonProperty("failUrl")]
        [Required]
        public string FailUrl { get; set; }

        [JsonProperty("cancelUrl")]
        [Required]
        public string CancelUrl { get; set; }
        
        [JsonProperty("bankType")]
        [Required]
        public BankType BankType { get; set; }
        
        [JsonProperty("startTerm")]
        [Required]
        public StartTerm StartTerm { get; set; }
        
        [JsonProperty("endTerm")]
        [Required]
        public EndTerm EndTerm { get; set; }

        [JsonProperty("bgColor")]
        public string BgColor => "#43a047";

        [JsonProperty("txtColor")]
        public string TxtColor => "#FFFFFF";
    }
}