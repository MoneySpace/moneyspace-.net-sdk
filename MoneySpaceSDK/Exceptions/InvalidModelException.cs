namespace MoneySpaceSDK.Exceptions
{
    public class InvalidModelException : MoneySpaceException
    {
        public InvalidModelException(string message) : base(message)
        {
        }
    }
}