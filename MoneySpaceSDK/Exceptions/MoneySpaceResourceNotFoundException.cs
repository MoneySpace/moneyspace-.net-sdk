using System.Net;

namespace MoneySpaceSDK.Exceptions
{
    internal class MoneySpaceResourceNotFoundException : MoneySpaceApiException
    {
        /// <summary>
        ///     Create a new <see cref="MoneySpaceResourceNotFoundException" /> instance.
        /// </summary>
        public MoneySpaceResourceNotFoundException()
            : base(HttpStatusCode.NotFound)
        {
        }
    }
}