using System.Net;

namespace MoneySpaceSDK.Exceptions
{
    internal class MoneySpaceApiException : MoneySpaceException
    {
        /// <summary>
        ///     Creates a new <see cref="MoneySpaceApiException" /> instance.
        /// </summary>
        /// <param name="httpStatusCode">The HTTP status code of the API response.</param>
        /// <param name="requestId">The unique identifier of the API request.</param>
        /// <param name="additionalInformation">Additional details about the error.</param>
        public MoneySpaceApiException(HttpStatusCode httpStatusCode, string additionalInformation = null)
            : base(GenerateMessage(httpStatusCode, additionalInformation))
        {
            HttpStatusCode = httpStatusCode;
        }

        /// <summary>
        ///     Gets the HTTP status code of the API response.
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; }

        private static string GenerateMessage(HttpStatusCode httpStatusCode, string additionalInformation = null)
        {
            var message = $"The API response status code ({httpStatusCode}) does not indicate success.";

            if (!string.IsNullOrWhiteSpace(additionalInformation))
                return message + " " + additionalInformation;

            return message;
        }
    }
}