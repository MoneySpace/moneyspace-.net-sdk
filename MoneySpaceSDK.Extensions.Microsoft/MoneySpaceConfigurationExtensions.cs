using MoneySpaceSDK.Extensions.Microsoft;

namespace Microsoft.Extensions.Configuration
{
    /// <summary>
    /// MoneySpace SDK extensions for <see cref="Microsoft.Extensions.Configuration.IConfiguration"/>.
    /// </summary>
    public static class MoneySpaceConfigurationExtensions
    {
        /// <summary>
        /// Gets the options from the "MoneySpace" configuration section needed to configure the MoneySpace.net SDK for .NET Core.
        /// </summary>
        /// <param name="configuration">The configuration properties.</param>
        /// <returns>The MoneySpace options initialized with values from the provided configuration.</returns>
        public static MoneySpaceOptions GetMoneySpaceOptions(this IConfiguration configuration)
        {
            return configuration.GetSection("MoneySpace").Get<MoneySpaceOptions>();
        }
    }
}