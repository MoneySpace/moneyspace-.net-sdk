using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoneySpaceSDK;
using MoneySpaceSDK.Merchant.Model;
using MoneySpaceSDK.Merchant.Model.Enum;

namespace MoneySpaceSdkMvcSample.Controllers
{
    [Route("payments")]
    public class PaymentsController : Controller
    {
        private readonly IMoneySpaceApi _moneySpaceApi;
        
        public PaymentsController(IMoneySpaceApi moneySpaceApi)
        {
            _moneySpaceApi = moneySpaceApi;
        }
        
        // POST payments
        [HttpPost("")]
        public async Task<IActionResult> CreatePayment()
        {
            // Fake transaction details
            var createPaymentRequest = new CreatePaymentRequest
            {
                FirstName = "example",
                LastName = "example",
                Email = "example@test.com",
                Phone = "0888888888",
                Description = "T-shirt 001 ,pricing 100.25 baht",
                Address = "Address 111/22",
                Message = "After shipping, please share me the delivery receipt",
                Amount = 0.25,
                FeeType = FeeType.Include,
                CustomerOrderId = "Order_" + DateTime.UtcNow,
                GatewayType = GatewayType.Card,
                SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
                FailUrl = "https://www.moneyspace.net?status=fail",
                CancelUrl = "https://www.moneyspace.net?status=cancel"
            };

            // Try to create payment
            var createdPayment = await _moneySpaceApi.Merchant.CreateAsync(createPaymentRequest);

            // If something went wrong
            if (!createdPayment.IsSuccess)
            {
                return BadRequest(new { Error = createdPayment.Status });
            }

            // Transaction created successfully
            var createdLink = await _moneySpaceApi.Merchant.GetPaymentCardLinkAsync(createdPayment.TransactionId);

            return Ok( new { TransactionId = createdPayment.TransactionId, LinkPayment =  createdLink });
        }
        
        // POST payments/qr
        [HttpPost("qr")]
        public async Task<IActionResult> CreateQRPayment()
        {
            // Fake transaction details
            var createPaymentRequest = new CreatePaymentRequest
            {
                FirstName = "example",
                LastName = "example",
                Email = "example@test.com",
                Phone = "0888888888",
                Description = "T-shirt 001 ,pricing 100.25 baht",
                Address = "Address 111/22",
                Message = "After shipping, please share me the delivery receipt",
                Amount = 1.50,
                FeeType = FeeType.Include,
                CustomerOrderId = "Qr_" + DateTime.UtcNow,
                GatewayType = GatewayType.Qrnone
            };

            // Try to create payment
            var createdPayment = await _moneySpaceApi.Merchant.CreateAsync(createPaymentRequest);

            // If something went wrong
            if (!createdPayment.IsSuccess)
            {
                return BadRequest(new { Error = createdPayment.Status });
            }

            // Transaction created successfully
            var createdLink = await _moneySpaceApi.Merchant.GetPaymentCardLinkAsync(createdPayment.TransactionId);

            return Ok( new { TransactionId = createdPayment.TransactionId, LinkPayment =  createdLink });
        }

        // POST payments
        [HttpPost("installment")]
        public async Task<IActionResult> CreateInstallment([FromForm]BankType bankType)
        {
            // Fake transaction details
            var createInstallmentRequest = new CreateInstallmentRequest
            {
                FirstName = "example",
                LastName = "example",
                Email = "example@test.com",
                Phone = "0888888888",
                Description = "T-shirt 001 ,pricing 100.25 baht",
                Address = "Address 111/22",
                Message = "After shipping, please share me the delivery receipt",
                Amount = 3100.01,
                FeeType = FeeType.Include,
                CustomerOrderId = "Inst_" + DateTime.UtcNow,
                SuccessUrl = "https://www.moneyspace.net/merchantapi/paycardsuccess",
                FailUrl = "https://www.moneyspace.net?status=fail",
                CancelUrl = "https://www.moneyspace.net?status=cancel",
                BankType = bankType,
                StartTerm = StartTerm.Three,
                EndTerm = EndTerm.Ten
            };

            // Try to create payment
            var createdInstallment = await _moneySpaceApi.Merchant.CreateInstallmentAsync(createInstallmentRequest);

            // If something went wrong
            if (!createdInstallment.IsSuccess)
            {
                return BadRequest(new { Error = createdInstallment.Status });
            }
            
            // Transaction created successfully
            return Ok( new { TransactionId =  createdInstallment.TransactionId });
        }
        
        // GET payments/check-transaction
        [HttpGet("check-transaction")]
        public async Task<IActionResult> CheckTransaction([FromQuery] string transactionId)
        {
            var checkTransactionRequest = new CheckTransactionRequest
            {
                TransactionId = transactionId,
            };

            // Check transaction
            var checkTransactionResponse = await _moneySpaceApi.Merchant.CheckTransactionAsync(checkTransactionRequest);

            // If any error while checking
            if (!checkTransactionResponse.IsSuccess)
            {
                return BadRequest(new { Error = checkTransactionResponse.Status });
            }

            // Check was successful
            return Ok( checkTransactionResponse );
        }
        
        // POST payments/webhook-receiver
        [HttpPost("webhook-receiver")]
        public IActionResult WebHookReceiver([FromForm] WebHookResponse webHookResponse)
        {
            // Verify WebHook hash for integrity
            if (!_moneySpaceApi.WebHookHelper.VerifyResponse(webHookResponse))
            {
                // WebHook parameters is not real.
                return BadRequest( new { Error = "Error when verifying the WebHook request." }  );
            }
            
            // WebHook is true and verified.
            // Do what you want.
            switch (webHookResponse.Status)
            {
                case PaymentStatus.Ok:
                    
                    break;
                case PaymentStatus.Pending:
                    
                    break;
                case PaymentStatus.Fail:
                    
                    break;
                case PaymentStatus.Cancel:
                    
                    break;
                case PaymentStatus.PaySuccess:
                    
                    break;
            }

            return Ok();
        }
    }
}