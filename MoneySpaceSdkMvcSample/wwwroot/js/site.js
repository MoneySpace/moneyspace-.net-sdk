﻿$(document).ready(function() {
    $(".form-wrapper .button").click(function() {
        var button = $(this);
        var currentSection = button.parents(".section");
        var currentSectionIndex = currentSection.index();
        var headerSection = $('.steps li').eq(currentSectionIndex);
        currentSection.removeClass("is-active").next().addClass("is-active");
        headerSection.removeClass("is-active").next().addClass("is-active");

        $(".form-wrapper").submit(function(e) {
            e.preventDefault();
        });

        $('input[type=radio][name=payment_type]').change(function() {
            if (this.value == 'installment') {
                $("#installment_bank_1").fadeIn(1000);
                $("#installment_bank_2").fadeIn(1200);
            } else {
                $("#installment_bank_1").fadeOut(1200);
                $("#installment_bank_2").fadeOut(1000);
            }
        });


        if (currentSectionIndex === 1) {

            var payment_type = $("input[name=payment_type]:checked").val()


            if (payment_type === "card") {
                CreatePayment("/payments")
                    .then(response => {
                        var link_payment = response["linkPayment"];
                        var transaction_ID = response["transactionId"];
                        $(".payment").append("<h3>รหัสธุรกรรม : " + transaction_ID + "</h3>");
                        $(".payment").append("<a class='button' href='" + link_payment +
                            "'>ชำระเงิน</a>");
                        $("#loading").remove();
                        $("#wait").hide();

                    }).catch(error => {})
            }

            if (payment_type === "qr") {

                CreatePayment("/payments/qr")
                    .then(response => {
                        var transaction_ID = response["transactionId"];
                        $(".payment").append(
                            "<h3 id='h3_qr' >Please scan and pay with Mobile Banking within 15 minutes.</h3>"
                        );
                        $(".payment").append("<img id='qrprom' src='" + response["linkPayment"] + "'>");
                        $("#loading").remove();
                        $("#wait").hide();

                        CheckPayment(transaction_ID).then(data => {}).catch(error => {})

                    }).catch(error => {})
            }

            if (payment_type === "installment") {

                var bank = $("input[name=bank]:checked").val()


                CreatePayment("/payments/installment", bank)
                    .then(response => {

                        var transaction_ID = response['transactionId'];

                        var KTC = [3, 4, 5, 6, 7, 8, 9, 10];
                        var BAY_FCY = [3, 4, 6, 9, 10];

                        var permonth_3 = 4200 / 3;

                        $(".payment").append("<h3>เลือกเดือนในการผ่อนชำระ</h3>");


                        if (bank == "KTC") {

                            $(".payment").append(
                                '<form  id="payform"  method="post" action="https://www.moneyspace.net/ktccredit/payment/directpay"></form'
                            );
                            $("#payform").append(
                                '<select class="select-css" id="term" name="term" style="text-align-last:center;" onchange="termselect();"></select>'
                            );
                            $("#payform").append(
                                '<input type="hidden" id="transactionID" name="transactionID" value="' +
                                transaction_ID + '">');
                            $("#payform").append(
                                '<input type="hidden" id="pay_type" name="pay_type" value="">'
                            );
                            $("#payform").append(
                                '<input type="hidden" id="locale" name="locale" value="">');
                            $("#payform").append(
                                '<input type="hidden" id="paymonth" name="paymonth" value="' +
                                permonth_3.toFixed(2) + '">');
                            $("#payform").append(
                                '<input type="hidden" id="bankType"name="bankType" value="KTC">'
                            );
                            $("#payform").append(
                                '<input type="hidden" id="interest"name="interest" value="0.0">'
                            );
                            $("#payform").append(
                                '<button class="button" type="submit">ชำระเงิน</button>');

                            KTC.forEach(cal);

                            function cal(month, index) {
                                var permonth = 4200 / month;
                                $(".select-css").append('<option value="' + month + '">' +
                                    permonth
                                        .toFixed(2) + ' X ' + month + ' เดือน</option>')

                            }
                        }

                        if (bank == "BAY" || bank == "FCY") {

                            $(".payment").append(
                                '<form  id="payform"  method="post" action="https://www.moneyspace.net/baycredit/pay"></form'
                            );
                            $("#payform").append(
                                '<select class="select-css" id="term" name="term" style="text-align-last:center;" onchange="termselect();"></select>'
                            );
                            $("#payform").append(
                                '<input type="hidden" id="transactionID" name="transactionID" value="' +
                                transaction_ID + '">');
                            $("#payform").append(
                                '<input type="hidden" id="pay_type" name="pay_type" value="">'
                            );
                            $("#payform").append(
                                '<input type="hidden" id="locale" name="locale" value="">');
                            $("#payform").append(
                                '<input type="hidden" id="paymonth" name="paymonth" value="' +
                                permonth_3.toFixed(2) + '">');
                            $("#payform").append(
                                '<input type="hidden" id="bankType"name="bankType" value="' +
                                bank + '">');
                            $("#payform").append(
                                '<input type="hidden" id="interest"name="interest" value="0.0">'
                            );
                            $("#payform").append(
                                '<button class="button" type="submit">ชำระเงิน</button>');


                            BAY_FCY.forEach(cal);

                            function cal(month, index) {
                                var permonth = 4200 / month;
                                $(".select-css").append('<option value="' + month + '">' +
                                    permonth
                                        .toFixed(2) + ' X ' + month + ' เดือน</option>')
                            }
                        }

                        $("#loading").remove();
                        $("#wait").hide();

                    }).catch(error => {})
            }

        }

        if (currentSectionIndex === 3) {
            $(document).find(".form-wrapper .section").first().addClass("is-active");
            $(document).find(".steps li").first().addClass("is-active");
        }
    });
});

function CreatePayment(apiurl, selectbank) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: apiurl,
            type: 'POST',
            data: {
                'bankType': selectbank
            },
            success: function(data) {
                resolve(data)
            },
            error: function(error) {
                reject(error)
            },
        })
    })
}


function CheckPayment(transactionID) {

    var intervalID = setInterval(function() {

        new Promise((resolve, reject) => {
            $.ajax({
                url: "/payments/check-transaction?transactionId=" + transactionID,
                type: 'GET',
                success: function(res) {

                    if (res.statusPayment == "Pay Success") {
                        $("#qrprom").remove();

                        $("#h3_qr").text("ขำระเงินเรียบร้อยแล้ว !!");
                        $("#h3_qr").css("color", "green");

                        clearInterval(intervalID);
                    }

                },
                error: function(error) {
                    reject(error)
                },
            })
        })

    }, 1200);

}

function termselect() {

    var term = $("#term").val()

    var permonth = 4200 / term;

    $("#paymonth").val(permonth.toFixed(2));

}